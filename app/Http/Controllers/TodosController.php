<?php

namespace App\Http\Controllers;

use App\Todo;
use App\Http\Resources\TodoResource;
use Illuminate\Http\Request;

class ToDoController extends Controller
{
    public function store(Request $request)
    {
        $todo = new Todo();
        $todo->title = $request->title;
        $todo->desc = $request->desc;
        $todo->status = $request->status;
        $todo->save();
    }

    public function show(Request $request, $TodoId)
    {
        if (!$todoId) {
            return TodoResource::collection(Todo::all());
        }

        return new TodoResource(Todo::find($todoID));
    }

    public function update(Request $request, $todoID)
    {
        $todo = Todo::find($todoId);
        if ($request->title) {
            $todo->title = $request->title;
        }

        if ($request->desc) {
            $todo->desc = $request->desc;
        }

        if ($request->status) {
            $todo->status = $request->status;
        }

        $todo->save();

    }

}
