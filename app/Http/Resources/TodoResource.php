<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resources;

class TodoResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'desc' => $this->desc,
            'status' => $this->status
        ]
    }
}
